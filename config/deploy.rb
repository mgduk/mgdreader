site_name = "mgdreader"
set :application, "#{site_name}"
set :repository,  "git@bitbucket.org:mgduk/mgdreader.git"
set :scm,         :git
set :branch,	"master"
ssh_options[:forward_agent] = true

set :user, "#{site_name}"
set :deploy_to, "/home/mgdphoto/mgdreader"
set :use_sudo, false

set :deploy_via, :remote_cache
set :copy_exclude, [".git", ".DS_Store", ".gitignore", ".gitmodules", ".sass-cache", "sass"]

server "yew.hullabaloo.co.uk", :app, :web

set :htdocs_dir, "#{File.expand_path('.')}/htdocs"


namespace :mgd do
	after "deploy:update_code" do

		# compile compass
		run_locally	"cd #{htdocs_dir}; compass clean"
		run_locally	"cd #{htdocs_dir}; compass compile --environment production"

		# run_locally	"cd #{htdocs_dir}; coffee --compile --output htdocs/js htdocs/js"

		# ensure skin dir exists
        run "mkdir -p #{release_path}/htdocs/skin"

        # Upload
        Dir.glob("#{htdocs_dir}/skin/*.css").each do|f|
            upload f, "#{release_path}/htdocs/skin/#{File.basename(f)}"
        end
        Dir.glob("#{htdocs_dir}/js/*.js").each do|f|
            upload f, "#{release_path}/htdocs/js/#{File.basename(f)}"
        end

      # symlink to config
		run	"rm #{release_path}/user-config.json; ln -s #{shared_path}/user-config.json #{release_path}/user-config.json"
	end
end



namespace :apache do
  [:stop, :start, :restart, :reload].each do |action|
    desc "#{action.to_s.capitalize} Apache"
    task action, :roles => :web do
      invoke_command "sudo /etc/init.d/apache2 #{action.to_s}", :via => run_method
    end
  end
end

desc "Restart apache"
task :restart, :roles => :app, :except => { :no_release => true } do
  deploy.apache.reload
end

desc "Start Apache"
task :start, :roles => :app do
  deploy.apache.start
end

desc "Stop Apache"
task :stop, :roles => :app do
  deploy.apache.stop
end
