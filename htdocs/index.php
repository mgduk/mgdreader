<?php

namespace DolanReader;

include '../lib/core.php';

$app = new App(Config::get('rootURL'));

$app->addRoute('/feeds/([0-9]+)(?:/[^/]*)?/?',array($app,'feedView'));

$app->addRoute('/feeds/([0-9]+)(?:/[^/]*)?/edit',array($app,'feedEditView'));

$app->addRoute('/feeds/([0-9]+)/[^/]*/([0-9]+)(?:/[^/]*)?/?',array($app,'feedItemView'));

$app->addRoute('/',array($app,'listView'));

$app->addRoute('/unit-tests',array('DolanReader\UnitTests','runTests'));

try {
	$app->route($_SERVER['SCRIPT_NAME'],$_SERVER['REQUEST_METHOD'])->output();
} catch (\Exception $e) {
	new ErrorView(500,'',(string)$e);
}

?>