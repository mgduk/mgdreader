<?php
namespace DolanReader;

include '../lib/core.php';

$api = new ApiRouter('/api');

/*********************************************************************************************
**  GET
*********************************************************************************************/

// get all feeds
$api->addRoute('/feeds',function(){
    $feeds = Feed::getAllData();
    return new ApiResponse(200,$feeds);
},'get');


// get feed
$api->addRoute('/feeds/([0-9]+)',function($feedId){
    $feed = new Feed($feedId);
    if (!$feed->id)
        return new ApiErrorResponse(404,'Feed Not Found');
    return new ApiResponse(200,$feed->data);
},'get');

// get feed's items
$api->addRoute('/feeds/([0-9]+)/items',function($feedId){
    $feed = new Feed($feedId);
    if (!$feed->id)
        return new ApiErrorResponse(404,'Feed Not Found');
    return new ApiResponse(200,array_map(function($item){return $item->minimalData;},$feed->items));
},'get');


// get an item
$api->addRoute('/items/([0-9]+)',function($itemId){
    $item = FeedItem::get($itemId);
    if (!$item)
        return new ApiErrorResponse(404,'Item Not Found');
    return new ApiResponse(200,$item->data);
},'get');


// get feed's updates
$api->addRoute('/feeds/([0-9]+)/updates',function($feedId){
    $feed = new Feed($feedId);
    if (!$feed->id)
        return new ApiErrorResponse(404,'Feed Not Found');
    $output = array();
    foreach ($feed->updates as $update) {
        $updates[] = $update->data;
    }
    return new ApiResponse(200,$updates);
},'get');



/*********************************************************************************************
**  POST
*********************************************************************************************/

// create feed (add new RSS URL)
$api->addRoute('/feeds',function(){
    $url = $_POST['url'];
    if (empty($url)) {
        return new ApiErrorResponse(400,'Please specify a url');

    // only allow http(s) URLs
    } elseif (!preg_match('%^https?://%', $url)) {
        return new ApiErrorResponse(400,'RSS feed URLs must begin with http:// or https://');
    }

    // try and retrieve feed from database
    $feed = Feed::getByRssUrl($url);

    // if this feed is already in the database, redirect to it
    if ($feed) {
        return new ApiResponse(303,'Feed already in database',array('Location'=>$feed));

    // this is a new feed, go get it!
    } else {
        $feed = new Feed();
        $feed->rssUrl = $_POST['url'];
        FeedUpdate::update($feed);
        return new ApiResponse(201,$feed->title.' added',array('Location'=>$feed));
    }
},'post');



// create feed update
$api->addRoute('/feeds/([0-9]+)/updates',function($feedId){
    $feed = new Feed($feedId);
    if (!$feed->id)
        return new ApiErrorResponse(404,'Feed Not Found');

    $update = FeedUpdate::update($feed);

    $output = array();

    switch ($update->itemCount) {
        case 0: $message = 'Feed updated, no new items'; break;
        case 1: $message = 'Feed updated, 1 item added'; break;
        default: $message = 'Feed updated, '.$update->itemCount.' items added';
    }
    if ($update->pruneCount)
        $message .= sprintf(' (%d old item%s discarded)',
                        $update->pruneCount,
                        ($update->pruneCount==1)?'':'s'
                    );

    $output['message'] = $message;
    $output['itemCount'] = (integer)$update->itemCount;
    $output['pruneCount'] = (integer)$update->pruneCount;
    $output['feedUrl'] = $feed;

    return new ApiResponse(201,$output,array('Location'=>$update->apiUrl));
},'post');



/*********************************************************************************************
**  PUT
*********************************************************************************************/

// save feed details
$api->addRoute('/feeds/([0-9]+)',function($feedId){
    $feed = new Feed($feedId);

    $feed->data = $_REQUEST;

    $success = $feed->save();

    return new ApiResponse(200,'Saved',array('Location'=>$feed));
},'put');



/*********************************************************************************************
**  DELETE
*********************************************************************************************/

// save feed details
$api->addRoute('/feeds/([0-9]+)',function($feedId){
    $feed = new Feed($feedId);

    $success = $feed->delete();

    if ($success)
        return new ApiResponse(200,$feed->title.' deleted',array('Location'=>'/'));
    else
        return new ApiErrorResponse(500,'Error deleting '.$feed->title,array('Location'=>$feed));
},'delete');



/*********************************************************************************************/

// '_method' param can be used to define method as PUT, DELETE, etc
$method = ($_REQUEST['_method']) ? $_REQUEST['_method'] : $_SERVER['REQUEST_METHOD'];

$api->route($_SERVER['SCRIPT_NAME'],$method)->output();

?>