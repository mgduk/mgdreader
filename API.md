API endpoints
-------------

GET /api/feeds
Lists the feeds

GET /api/feeds/1
Represents the Feed 1, including the items that belong to that feed

GET /api/feeds/1/items
Represents the collection of items for Feed 1

GET /api/item/123
Represents the Item 123


POST /api/feeds
Create feed
Parameters:
	url	- HTTP(S) URL of the RSS feed

POST /api/feeds/1/updates
Initiates a new update for Feed 1


PUT /api/feeds/1
Modifies the data of Feed 1
Parameters:
	title
	link
	description
	language
	copyright
	image
	pubDate
	lastBuildDate
	id
	rssUrl
	data
