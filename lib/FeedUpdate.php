<?php
namespace DolanReader;

/**
 * A FeedUpdate represents a pull of the RSS feed
 *
 * If we're updating an existing Feed, the RSS will be parsed and new items added to the feed.
 * If we're updating a new Feed, the Feed's data will be extracted from the RSS too.
 */
class FeedUpdate {

	protected $id;
	protected $feedId;
	protected $feed;
	protected $xml;
	protected $date;
	protected $itemCount;

	protected $pruneCount;

	/**
	 * Returns the Feed object associated with this FeedUpdate
	 * @return Feed
	 */
	protected function getFeed () {
	    if (!$this->feed) {
	        $this->feed = new Feed($this->feedId);
	    }
	    return $this->feed;
	}


	/**
	 * Returns an array representing this Feed's data
	 * @return array
	 */
	protected function getData () {
		return array(
			'id' => (integer)$this->id,
			'date' => ($this->date)?$this->date->format('c'):null,
			'itemCount' => (integer)$this->itemCount
		);
	}

	/**
	 * Sets the Feed object, and ensures that $feedId is set
	 * @param Feed $feed
	 */
	public function setFeed ($feed) {
		$this->feed = $feed;
		$this->feedId = $feed->id;
	}

	/**
	 * Set data, e.g. from database
	 * @param array $data
	 */
	public function setData ($data) {
		$this->feedId = $data['feed'];
		$this->id = $data['id'];
		$this->date = ($data['date']) ? new \DateTime($data['date']) : null;
		$this->itemCount = $data['itemCount'];
	}

	/**
	 * Loads the RSS feed from the Feed's RSS URL
	 * @return SimpleXmlElement
	 */
	public function retrieveRss () {
		try {
			$this->xml = simplexml_load_file($this->feed->rssUrl);
		} catch (\Exception $e) {
			$message = (DEBUGGING) ? (string)$e : "Unable to retrive RSS feed. Please check the URL and try again.";
			throw new \Exception($message, 1);
		}
		return $this->xml;
	}

	/**
	 * Reads the channel data from the RSS feed into the Feed object and saves
	 * @return boolean	TRUE for success
	 */
	protected function updateFeed () {
		try {
			$data = (array)$this->xml->channel->children();
			if ($data['image'])
				$data['image'] = (string)$data['image']->url;
			$this->feed->data = $data;

			return $this->feed->save();
		} catch (\Exception $e) {
			if (DEBUGGING)
				throw $e;
			else
				throw new \Exception("Error processing feed", 1);
		}
	}

	/**
	 * Parses items from XML and stores them in the database
	 *
	 * Items that are already in the database won't be duplicated
	 * @return integer	Number of items added
	 */
	protected function updateItems () {
		try {
			// handle variation in feed XML structure
			$itemsXml = (count($this->xml->item)) ? $this->xml->item : $this->xml->channel->item;

			$itemCountBeforeUpdate = $this->feed->itemCount;

			// get namespaces used in document
			$namespaces = $itemsXml->getNameSpaces(true);

			$count = 0;
			foreach ($itemsXml as $itemXml) {
				$item = new FeedItem($this->feed);
				$data = get_object_vars($itemXml);

				// merge in dc: namespaced elements
				if (isset($namespaces['dc'])) {
					$dc = $itemXml->children($namespaces['dc']);
					$data += get_object_vars($dc);
				}

				// merge in media: namespaced elements
				if (isset($namespaces['media'])) {
					$media = $itemXml->children($namespaces['media']);
					foreach ($media->thumbnail as $thumbnail) {
						$image = $thumbnail->attributes();
						if ($image['url'])
							$data['image'] = (string)$image['url'];
					}
				}


				// if GUID is a permalink, use that as link
				if (!isset($data['link']) && (string)$itemXml->guid['isPermaLink']==='true')
					$data['link'] = $data['guid'];

				$item->setData($data);
				$item->save();

				if ($data['enclosure']) {
					$enclosure = new Enclosure($item);
					$enclosure->data = $data['enclosure'][0]->attributes();
					$enclosure->save();
				}

				$count++;
			}
		} catch (\Exception $e) {
			if (DEBUGGING)
				throw $e;
			else
				throw new \Exception("Error retrieving feed items", 1);
		}

		$itemCountAfterUpdate = $this->feed->itemCount;

		$this->itemCount = $itemCountAfterUpdate - $itemCountBeforeUpdate;

		return $this->itemCount;
	}

	/**
	 * Deletes items older than X days (where X is defined in the configuration)
	 * @return integer	number of items deleted
	 */
	protected function pruneOldItems () {
		$maxAge = Config::get('maxAge');
		if (!$maxAge) return;

		$db = DB::get();
		$query = $db->prepare('DELETE FROM `items` WHERE feed = :feed AND DATE_ADD(`dateAdded`,INTERVAL :maxAge DAY)<NOW()');
		$success = $query->execute(array('feed'=>$this->feedId,'maxAge'=>$maxAge));
		$this->pruneCount = ($success) ? $query->rowCount() : null;

		return $this->pruneCount;
	}

	/**
	 * Populates this object with data from the database
	 * @param  integer $id FeedUpdate ID
	 * @return boolean	Success or failure
	 */
	protected function loadData ($id) {
		if ($this->dataLoaded) return true;

		$this->id = $id;

		$db = Db::get();

		$query = $db->prepare("SELECT * FROM `updates` WHERE `id` = :id LIMIT 1");
		$query->bindParam('id',$this->id);

		$success = $query->execute();

		# setting the fetch mode
		$data = $query->fetch(\PDO::FETCH_ASSOC);
		if ($data) {
			$this->setData($data);
		}

		return $success && $query->rowCount();
	}

	/**
	 * Saves this object's database into the `updates` table
	 * @return boolean	Success or failure
	 */
	protected function save () {
		$db = Db::get();

		if (!$this->feedId)
			$this->feedId = $this->feed->id;
		if (!$this->feedId)
			return false;

		$query = $db->prepare("INSERT INTO `updates` (`feed`, `date`,`itemCount`)
									VALUES (:feed, NOW(),:itemCount)");

		$query->bindParam('feed',$this->feedId);
		$query->bindParam('itemCount',$this->itemCount);

		$success = $query->execute();

		if ($success)
			$this->id = $db->lastInsertId();

		return $success;
	}

	public function __get ($var) {
		switch ($var) {
			case 'feed':      		return $this->getFeed();
			case 'id':        			return $this->id;
			case 'date':      		return $this->date;
			case 'itemCount': 	return $this->itemCount;
			case 'pruneCount':	return $this->pruneCount;
			case 'url':       			return $this->getUrl();
			case 'apiUrl':       			return $this->getApiUrl();
			case 'data':       			return $this->getData();
		}
	}

	public function __set ($var,$value) {
		switch ($var) {
			case 'data': $this->setData($value); break;
		}
	}

	/**
	 * There is no user-facing view for a FeedUpdate, so the Feed's URL is most appropriate
	 * @return string
	 */
	public function getUrl () {
		return $this->feed->url;
	}

	/**
	 * Return the API endpoint for this update
	 * @return string
	 */
	public function getApiUrl () {
		return $this->feed->apiUrl.'/updates/'.$this->id;
	}


	public function __construct ($id = null) {
		if ($id)
			$this->loadData($id);
	}

	/**
	 * Creates a new FeedUpdate object for the given feed, and performs the update process
	 * @param  Feed $feed
	 * @return FeedUpdate
	 */
	public static function update (Feed $feed) {
		$update = new FeedUpdate();
		$update->setFeed($feed);
		$xml = $update->retrieveRss();
		if (!$update->feed->id)
			$update->updateFeed();
		$update->updateItems();
		$update->pruneOldItems();
		$update->save();
		return $update;
	}
}

?>