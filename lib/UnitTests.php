<?php
namespace DolanReader;

class UnitTests {

	protected static function assertArrays ($test,$expected) {
		$failures = array();
		foreach ($expected as $key=>$value) {
			if ($test[$key]!==$value)
				$failures[] = "  Assertion failed for ".$key." (".$test[$key].")";
		}
		if ($failures)
			print implode("\n",$failures);
		else
			print "  All ok\n";
	}

	/**
	 * Deletes feed from database
	 * @return [type] [description]
	 */
	protected static function prepFeed () {
		$testFeedUrl = realpath(Config::getFolder('lib').'../tests/test.xml');
		$db = DB::get();

		$feed = Feed::getByRssUrl($testFeedUrl);

		if ($feed) {
			// delete items
			$query = $db->prepare("DELETE FROM items WHERE feed = :feed");
			$query->bindParam('feed',$feed->id);
			$query->execute();

			// delete feed
			$query = $db->prepare("DELETE FROM feeds WHERE id = :feed");
			$query->bindParam('feed',$feed->id);
			$query->execute();
		}

		$feed = Feed::getByRssUrl($testFeedUrl);
		if ($feed)
			die("Feed still exists after attempted delete");

		$feed = new Feed();
		$feed->rssUrl = $testFeedUrl;
		return $feed;
	}

	/**
	 * Tests the parsing of XML and storing in database
	 */
	public static function addFeed () {
		$feed = self::prepFeed();
		$update = FeedUpdate::update($feed);

		$db = DB::get();
		// feed details
		$query = $db->prepare("SELECT * FROM feeds WHERE id = :feed");
		$query->bindParam('feed',$update->feed->id);
		$success = $query->execute();
		$row = $query->fetch(\PDO::FETCH_ASSOC);

		$expectedData = array(
			'title' => 'Test RSS feed',
			'link' => 'http://www.example.com/feed.rss',
			'description' => 'Feed description',
			'language' => 'en-gb',
			'lastBuildDate' => '2013-07-14 19:51:47',
			'copyright' => 'Not applicable',
			'image' => 'http://www.example.com/logo_120x60.gif'
		);

		print "\nTesting Add Feed…\n";
		self::assertArrays($row,$expectedData);

		// items
		$query = $db->prepare("SELECT * FROM items WHERE feed = :feed");
		$query->bindParam('feed',$update->feed->id);
		$success = $query->execute();
		$item1 = $query->fetch(\PDO::FETCH_ASSOC);
		$item2 = $query->fetch(\PDO::FETCH_ASSOC);

		print "  Checking Item 1…\n";
		self::assertArrays($item1,array(
			'title' => 'Story 1',
			'description' => 'Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum.',
			'link' => 'http://www.example.com/story-1',
			'guid' => 'http://www.example.com/story-1',
			'date' => '2013-07-14 21:18:11',
			'image' => 'http://www.example.com/story-1.jpg'
		));

		print "  Checking Item 2…\n";
		self::assertArrays($item2,array(
			'title' => 'Story 2',
			'description' => 'Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Sed posuere consectetur est at lobortis.',
			'link' => 'http://www.example.com/story-2',
			'guid' => 'http://www.example.com/story-2',
			'date' => '2013-07-14 20:04:00',
			'image' => 'http://www.example.com/story-2.jpg'
		));

	}

	/**
	 * Change the feed data
	 * @return [type] [description]
	 */
	public static function updateFeed () {
		$feed = self::prepFeed();
		// create feed
		$update = FeedUpdate::update($feed);

		$newData = array(
			'title' => 'Updated test RSS feed',
			'link' => 'http://www.example.net/feed.rss',
			'description' => 'Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.',
			'language' => 'en-us',
			'lastBuildDate' => '2013-07-15 19:51:47',
			'copyright' => 'Whatever',
			'image' => 'http://www.example.net/logo_120x60.gif'
		);

		$feed->data = $newData;
		$feed->save();

		$db = DB::get();
		// feed details
		$query = $db->prepare("SELECT * FROM feeds WHERE id = :feed");
		$query->bindParam('feed',$feed->id);
		$success = $query->execute();
		$row = $query->fetch(\PDO::FETCH_ASSOC);

		print "\nTesting Update Feed…\n";
		self::assertArrays($row,$newData);
	}


	public static function deleteFeed () {
		$feed = self::prepFeed();

		$update = FeedUpdate::update($feed);

		$db = DB::get();
		// feed details
		$query = $db->prepare("SELECT * FROM feeds WHERE id = :feed");
		$query->bindParam('feed',$feed->id);
		$query->execute();

		print "\nTesting Delete feed…\n";
		if (!$query->rowCount())
			die("Feed wasn't created properly");

		$feed->delete();

		$query = $db->prepare("SELECT * FROM feeds WHERE id = :feed");
		$query->bindParam('feed',$feed->id);
		$query->execute();

		if ($query->rowCount()>0)
			print "  Deletion failed\n";

		print "  All ok\n";
	}

	public static function runTests () {
		if (!DEBUGGING)
			new ErrorView(403,'Access denied.');

		header('Content-type: text/plain; charset=utf8');

		ob_start();
		print "Unit Tests\n";
		UnitTests::addFeed();
		UnitTests::updateFeed();
		UnitTests::deleteFeed();
		ob_end_flush();
	}

}

?>