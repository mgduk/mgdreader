<?php
namespace DolanReader;

/**
 * Returns a HTTP redirection code to change location, and then terminates execution
 */
class Redirect {

	public function __construct ($url,$code = 303) {
		switch ($code) {
			case 301: header('HTTP/1.1 301 Moved Permanently'); break;
			case 302: header('HTTP/1.1 302 Found'); break;
			default: header('HTTP/1.1 303 See Other'); break;
		}
		header('Location: '.$url);
		die('Redirecting to '.htmlspecialchars($url));
	}
}

?>