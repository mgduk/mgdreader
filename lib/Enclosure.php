<?php
namespace DolanReader;

/**
 * Represents media attached to a FeedItem
 */
class Enclosure {

	protected $item;
	protected $url;
	protected $type;
	protected $length;

	public function __get ($var) {
		switch ($var) {
			case 'item': return $this->item;
			case 'url': return $this->url;
			case 'type': return $this->type;
			case 'title': return $this->getTitle();
			case 'length': return $this->length;
			case 'data': return $this->getData();
		}
	}

	public function __set ($var,$value) {
		switch ($var) {
			case 'url':   	$this->url = $value; break;
			case 'type':  	$this->type = $value; break;
			case 'length':	$this->length = $value; break;
			case 'data':  	$this->setData($value); break;
		}
	}

	/**
	 * Returns a human-readable title
	 * @return string
	 */
	protected function getTitle () {
		switch ($this->type) {
			// some common types
			case 'image/jpeg': $type = 'JPEG Image'; break;
			case 'image/gif': $type = 'GIF Image'; break;
			case 'audio/x-m4a': $type = 'MPEG-`4 Audio'; break;

			// attempt make other types more human readable
			default:
				$typeBits = array_reverse(explode('/',$this->type));
				array_walk($typeBits,function(&$s){$s=ucfirst($s);});
				$type = implode(' ',$typeBits);
		}
		return $type.': '.basename($this->url);
	}

	protected function getData () {
		return array(
			'url' => $this->url,
			'type' => $this->type,
			'title' => $this->title,
			'length' => $this->length
		);
	}

	protected function setData ($data) {
		foreach ($data as $key=>$value) {
			$this->__set($key,$value);
		}
	}

	public function save () {
		$db = Db::get();
		$query = $db->prepare("INSERT INTO `enclosures` (`item`,`url`,`type`,`length`)
								VALUES (:item,:url,:type,:length)");
		$success = $query->execute(array(
		        'item' => $this->item->id,
		        'url' => $this->url,
		        'type' => $this->type,
		        'length' => $this->length
		    ));
		if ($db->lastInsertId())
		    $this->id = $db->lastInsertId();

		return $success;
	}

	public function __construct ($item) {
		$this->item = $item;
	}

}

?>