<?php
namespace DolanReader;

class Message {

	const SESSION_KEY = 'sessionMessages';

	protected $message;
	protected $class;

	protected function getClass () {
		return rtrim('message '.$this->class);
	}

	protected function store () {
		if (!$_SESSION[self::SESSION_KEY])
			$_SESSION[self::SESSION_KEY] = array();
		$_SESSION[self::SESSION_KEY][] = $this;
	}

	public function output () {
		return sprintf('<div class="%s">%s</div>',
					$this->getClass(),
					$this->message
				);
	}

	public function __construct ($message,$class='') {
		$this->message = $message;
		$this->class = $class;
		$this->store();
	}

	/**
	 * Returns all stored messages, optionally clearing
	 * @param  boolean $clear If TRUE, all the messages will be discarded once returned
	 * @return Array of strings
	 */
	public static function get ($clear = true) {
		if (!isset($_SESSION[self::SESSION_KEY])) return array();

		$output = array();
		foreach ($_SESSION[self::SESSION_KEY] as $message) {
			if ($message->output())
				$output[] = $message->output();
		}
		if ($clear)
			unset($_SESSION[self::SESSION_KEY]);

		return $output;
	}
}

?>