<?php

namespace DolanReader;

/**
 * A full-page error message
 *
 * The view is immediately output, and then execution is terminated
 */
class ErrorView extends PageView {

	protected $message;
	protected $debugMessage;

	public function __construct ($code,$message,$debugMessage = null) {
		$this->message = $message;

		if (DEBUGGING && $debugMessage)
			$this->message = sprintf(
				'<div>Seeing as you\'re in debug mode, here\'s the full story:</div><pre>%s</pre><div>%s</div>',
				$debugMessage,
				$message
			);

		switch ($code) {
			case '403':
				$view = 'error403';
				header('HTTP/1.1 403 Forbidden');
				break;
			case '404':
				$view = 'error404';
				header('HTTP/1.1 404 File not found');
				break;
			default:
				$view = 'error500';
				header('HTTP/1.1 500 Internal server error');
		}

		parent::__construct($view);
		$this->title = 'Oops... '.$this->title;

		$this->output();
		die();
	}

}

?>