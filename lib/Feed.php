<?php

namespace DolanReader;

class Feed implements Viewable {

	protected static $feeds;

	protected $rssUrl;

	protected $id;
	protected $title;
	protected $link;
	protected $description;
	protected $language;
	protected $pubDate;
	protected $lastBuildDate;
	protected $copyright;
	protected $image;

	protected $items;

	protected $dataLoaded = false;

	public function __get ($var) {
		switch ($var) {
			case 'rssUrl': return $this->rssUrl;
			case 'id': return $this->id;
			case 'title': return $this->title;
			case 'link': return $this->link;
			case 'url': return $this->getUrl();
			case 'apiUrl': return $this->getApiUrl();
			case 'description': return $this->description;
			case 'language': return $this->language;
			case 'pubDate': return $this->pubDate;
			case 'lastBuildDate': return $this->lastBuildDate;
			case 'copyright': return $this->copyright;
			case 'image': return $this->image;
			case 'items': return $this->getItems();
			case 'itemCount': return $this->getItemCount();
			case 'data': return $this->getData();
			case 'minimalData': return $this->getData(false);
			case 'updates': return $this->getUpdates();
			case 'lastUpdate': return $this->getLastUpdate();
		}
	}

	public function __set ($var,$value) {
		switch ($var) {
			case 'title':
			case 'link':
			case 'description':
			case 'language':
			case 'copyright':
			case 'image':
			case 'rssUrl':
				$this->$var = $value;
				break;

			// don't create a DateTime with an empty value because it
			// uses the current time
			case 'pubDate':
			case 'lastBuildDate':
				$this->$var = (!empty($value)) ? new \DateTime($value) : null;
				break;

			// only set if not already set
			case 'id':
				if (empty($this->$var))
					$this->$var = $value;
				break;

			case 'data':
				$this->setData($value);
				break;
		}
	}

	/**
	 * Returns the URL for this Feed
	 * @return string
	 */
	public function getUrl () {
		return '/feeds/'.$this->id.'/'.urlify(strtolower($this->title));
	}

	/**
	 * Returns the API URL for this Feed
	 * @return string
	 */
	public function getApiUrl () {
		return '/api/feeds/'.$this->id;
	}

	/**
	 * Returns an array representing this Feed's data
	 * @return array
	 */
	protected function getData ($full = true) {
		$data = array(
			'id' => (integer)$this->id,
			'title' => $this->title,
			'link' => $this->link,
			'url' => $this->getApiUrl(),
			'lastBuildDate' => ($this->lastBuildDate)?$this->lastBuildDate->format('c'):null,
			'image' => $this->image
		);
		if ($full) {
			$data['rssUrl'] = $this->rssUrl;
			$data['description'] = $this->description;
			$data['language'] = $this->language;
			$data['pubDate'] = ($this->pubDate)?$this->pubDate->format('c'):null;
			$data['copyright'] = $this->copyright;
		}
		return $data;
	}

	/**
	 * Returns the FeedItems for this Feed, newest first
	 *
	 * FeedItems are retrieved from the database on request, then stored for future access.
	 * @return array of FeedItem
	 */
	protected function getItems () {
		if (!$this->items) {
			$db = Db::get();
			$query = $db->prepare("SELECT * FROM `items` WHERE `feed` = :feed ORDER BY `date` DESC");
			$query->bindParam('feed',$this->id);
			$query->execute();
			$this->items = array();
			while ($itemData = $query->fetch(\PDO::FETCH_ASSOC)) {
				$this->items[] = new FeedItem($this,$itemData);
			}
		}
		return $this->items;
	}

	/**
	 * Returns a specific FeedItem from this Feed by Id
	 * @param  integer $itemId FeedItem ID
	 * @return FeedItem or false on failure
	 */
	public function getItem ($itemId) {
		$db = Db::get();
		$query = $db->prepare("SELECT * FROM `items` WHERE `feed` = :feed AND `id` = :item LIMIT 1");
		$success = $query->execute(array('feed'=>$this->id,'item'=>$itemId));
		if (!$success || !$query->rowCount()) return false;
		$itemData = $query->fetch(\PDO::FETCH_ASSOC);
		$item = new FeedItem($this,$itemData);
		return $item;
	}

	/**
	 * Returns the number of items stored for this feed
	 * @return integer
	 */
	protected function getItemCount () {
		$db = Db::get();
		$query = $db->prepare("SELECT COUNT(*) AS `count` FROM `items` WHERE `feed` = :feed");
		$query->execute(array('feed'=>$this->id));
		$row = $query->fetch(\PDO::FETCH_ASSOC);
		return $row['count'];
	}

	/**
	 * Returns an array of FeedUpdate objects for this feed, newest first
	 * @return array of FeedUpdate
	 */
	protected function getUpdates () {
	    if (!$this->updates) {
	    	$db = Db::get();
	    	$query = $db->prepare("SELECT * FROM `updates` WHERE `feed` = :feed ORDER BY `date` DESC");
	    	$query->execute(array('feed'=>$this->id));
	        $this->updates = array();
	    	while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
	    		$update = new FeedUpdate();
	    		$update->data = $row;
	    		$this->updates[] = $update;
	    	}
	    }
	    return $this->updates;
	}

	/**
	 * Returns the most recent FeedUpdate object for this Feed
	 * @return FeedUpdate
	 */
	protected function getLastUpdate () {
		$this->getUpdates();
		return ($this->updates) ? reset($this->updates) : null;
	}

	/**
	 * Process the data in $data and set properties on this object
	 *
	 * Reusing __set() to keeps things DRY
	 * @param array $data
	 */
	protected function setData ($data) {
		if (is_array($data)) {
			foreach ($data as $key=>$value) {
				 $this->__set($key,$value);
			}
			$this->dataLoaded = true;
		}
	}


	/**
	 * Loads data from database
	 * @return boolean TRUE if successfully loaded
	 */
	public function loadData ($id) {
		if ($this->dataLoaded) return true;

		$this->id = $id;

		$db = Db::get();

		$query = $db->prepare("SELECT * FROM feeds WHERE `id` = :id LIMIT 1");
		$query->bindParam('id',$this->id);

		$success = $query->execute();

		# setting the fetch mode
		$data = $query->fetch(\PDO::FETCH_ASSOC);
		if ($data) {
			$this->id = $data['id'];
			$this->setData($data);
		}

		return $success && $query->rowCount();
	}

	/**
	 * Creates a new Feed from RSS XML at $url
	 * @param  string $url HTTP(S) URL of RSS feed
	 * @return Feed
	 */
	public function retrieveFeed ($url) {

		$xml = $this->getFeedXml();

		if (!$xml)
			throw new \Exception(
				sprintf('Failed to retrieve RSS feed from <a href="%s">%s</a>. Please check it and try again.',
						addslashes($url),
						htmlspecialchars($url)
					)
				, 1);

		// while we have the XML, load items too
		$this->retrieveItems($xml);

		return $this;
	}

	/**
	 * Save this Feed to the database
	 * @return boolean Success
	 */
	public function save () {
		$db = Db::get();
		if ($this->id) {
			$query = $db->prepare("UPDATE feeds SET
								`rssUrl`=:rssUrl,
								`title`=:title,
								`link`=:link,
								`description`=:description,
								`language`=:language,
								`pubDate`=:pubDate,
								`lastBuildDate`=:lastBuildDate,
								`copyright`=:copyright,
								`image`=:image
								WHERE `id` = :id"
							);
			// only bind the required parameters
			$success = $query->execute(array_intersect_key($this->getData(),array(
				'rssUrl' => true,
				'title' => true,
				'link' => true,
				'description' => true,
				'language' => true,
				'pubDate' => true,
				'lastBuildDate' => true,
				'copyright' => true,
				'image' => true,
				'id' => true
			)));
		} else {
			$query = $db->prepare("INSERT INTO feeds (
									`rssUrl`,
									`title`,
									`link`,
									`description`,
									`language`,
									`pubDate`,
									`lastBuildDate`,
									`copyright`,
									`image`,
									`dateAdded`
								) VALUES (
									:rssUrl,
									:title,
									:link,
									:description,
									:language,
									:pubDate,
									:lastBuildDate,
									:copyright,
									:image,
									NOW()
								)"
							);
			// only bind the required parameters
			$success = $query->execute(array_intersect_key($this->getData(),array(
				'rssUrl' => true,
				'title' => true,
				'link' => true,
				'description' => true,
				'language' => true,
				'pubDate' => true,
				'lastBuildDate' => true,
				'copyright' => true,
				'image' => true
			)));
		}

		if ($success && !$this->id)
			$this->id = $db->lastInsertId();
		return $success;
	}

	/**
	 * Deletes this feed and all items from the database
	 * @return boolean Success/failure
	 */
	public function delete () {
		if (!$this->id) return false;
		$db = Db::get();
		$query = $db->prepare("DELETE FROM feeds WHERE `id` = :feed LIMIT 1");
		$query->bindParam('feed',$this->id);
		$success = $query->execute();

		if (!$success)
			throw new \Exception("Database error deleting feed", 1);

		$query = $db->prepare("DELETE FROM items WHERE `feed` = :feed");
		$query->bindParam('feed',$this->id);
		$success = $query->execute();

		return $success;
	}

	/**
	 * Constructor. If a Feed ID number is passed, its data will be retrieved from the
	 * database to populate the object.
	 *
	 * @param integer $id
	 */
	public function __construct ($id = null) {
		if ($id) {
			$this->loadData($id);
		}
	}

	/*********************************************************************************************
	** Static methods
	*********************************************************************************************/

	/**
	 * Returns an array of all Feeds in the database
	 * @return array of Feed
	 */
	public static function getAll () {
		if (!self::$feeds) {
			$db = Db::get();
			try {
				$query = $db->query("SELECT * FROM feeds ORDER BY title");

				self::$feeds = array();
				while($data = $query->fetch(\PDO::FETCH_ASSOC)) {
					$feed = new Feed();
					$feed->data = $data;
				    self::$feeds[] = $feed;
				}
			} catch (\PDOException $e) {
				return false;
			}
		}
		return self::$feeds;
	}

	/**
	 * Returns an array containing an array of each feed's data
	 * @return array of array
	 */
	public static function getAllData () {
		self::getAll();
		$output = array();
		foreach (self::$feeds as $feed) {
			$output[] = $feed->minimalData;
		}
		return $output;
	}

	/**
	 * Retrieves a Feed object from the database by its RSS URL
	 *
	 * @param  string $rssUrl URL of the RSS feed
	 * @return Feed or FALSE on failure
	 */
	public static function getByRssUrl ($rssUrl) {
		$db = Db::get();
		$query = $db->prepare("SELECT * FROM feeds WHERE `rssUrl` = :rssUrl LIMIT 1");
		$query->bindParam('rssUrl',$rssUrl);

		$success = $query->execute();

		if (!$success || !$query->rowCount()) return false;

		$data = $query->fetch(\PDO::FETCH_ASSOC);

		if (!$data) return false;

		$feed = new Feed();
		$feed->setData($data);

		return $feed;
	}

}

?>