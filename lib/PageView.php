<?php

namespace DolanReader;

/**
 * A PageView is a full HTML page view
 *
 * The view specified is wrapped in a full HTML page, including <html> and <head>
 *
 * The outer HTML is defined in views/includes/pageShell.php
 */
class PageView extends View {

	protected $content;
	protected $title;

	public function output () {
		ob_start();
		extract($this->vars);
		try {
			include Config::getFolder('views').$this->view.'.php';
		} catch (\Exception $e) {
			if (strpos($e->getMessage(),'No such file'))
				new ErrorView(404,$this->view.' view not available.',(string)$e);
			else
				throw $e;
		}
		$this->content = ob_get_contents();
		ob_end_clean();
		include Config::getFolder('views').'includes/pageShell.php';
	}

	public function __set ($var,$value) {
		switch ($var) {
			case 'title': $this->title = $value; break;
		}
	}

	public function __construct ($view,$vars = array()) {
		$this->title = Config::get('title');
		parent::__construct($view,$vars);
	}

}

?>