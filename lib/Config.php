<?php

namespace DolanReader;

class Config {

	protected static $config;

	// path of app folder
	protected static $rootPath;

	protected static function jsonError () {
		switch (json_last_error()) {
	        case JSON_ERROR_NONE:
		        return;
	        case JSON_ERROR_DEPTH:
	            return 'Maximum stack depth exceeded';
		        break;
	        case JSON_ERROR_STATE_MISMATCH:
	            return 'Underflow or the modes mismatch';
		        break;
	        case JSON_ERROR_CTRL_CHAR:
	            return 'Unexpected control character found';
		        break;
	        case JSON_ERROR_SYNTAX:
	            return 'Syntax error, malformed JSON';
		        break;
	        case JSON_ERROR_UTF8:
	            return 'Malformed UTF-8 characters, possibly incorrectly encoded';
		        break;
	        default:
	            return 'Unknown error';
		        break;
	    }
	}

	/**
	 * Loads configuration from user config file, and mixes in defaults
	 *
	 * In a production environment, this would would be better memcached rather than decoded each time
	 * @return array
	 */
	protected static function loadConfig () {
		$defaultString = file_get_contents("../default-config.json");
		$userString = file_get_contents("../user-config.json");
		$defaultConfig = ($defaultString) ? json_decode($defaultString,true) : array();
		$userConfig = ($userString) ? json_decode($userString,true) : array();

		// if there's an error at this stage, we can't handle it more gracefully than just dying
		if ($defaultConfig===null)
			die("There is an error in the default-config.json file: ".self::jsonError());
		if ($userConfig===null)
			die("There is an error in your user-config.json file: ".self::jsonError());

		self::$config = $defaultConfig;

		// merge configs (using foreach rather than array_merge to allow merging arrays one level deep)
		if (is_array($userConfig))
			foreach ($userConfig as $key=>$value) {
				if (isset(self::$config[$key]) && is_array(self::$config[$key])
					&& isset($userConfig[$key]) && is_array($userConfig[$key])) {
					self::$config[$key] = array_merge(self::$config[$key],$userConfig[$key]);
				} else {
					self::$config[$key] = $value;
				}
			}

		return self::$config;
	}

	/**
	 * Returns a config variable
	 * @param  string $var Config variable name, returns $config[$var]
	 * @param  string $key Array key (assuming $var is an array, returns $config[$var][$key])
	 * @return Mixed
	 */
	public static function get ($var,$key=null) {
		if (!self::$config) {
			self::loadConfig();
		}
		if ($key===null)
			return self::$config[$var];
		elseif (isset(self::$config[$var][$key]))
			return self::$config[$var][$key];
	}

	/**
	 * Returns a fully qualified path to the specified folder-* variable,
	 * with a guaranteed trailing slash
	 * @param  string $folder Name of folder
	 * @return string
	 */
	public static function getFolder ($folderName) {
		$folder = self::get('folders',$folderName);
		if (empty($folder))
			return $folder;
		if (!self::$rootPath)
			self::$rootPath = rtrim(realpath($_SERVER['DOCUMENT_ROOT'].'/..'),'/').'/';
		return rtrim(self::$rootPath.$folder,'/').'/';
	}

	/**
	 * Private constructor because you can't instantiate this class.
	 * Use static Config::get() to get config a variable
	 */
	private function __construct () {}

}

?>