<?php

namespace DolanReader;

include 'utils.php';
include 'Config.php';

define('DEBUGGING',!!Config::get('debugMode'));

spl_autoload_register(function($class){
	if (strpos($class, 'DolanReader\\')===0) {
		 include Config::getFolder('lib').substr($class,12).'.php';
	}
});

session_start();

set_error_handler(array('DolanReader\App','errorHandler'));

?>