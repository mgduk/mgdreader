<?php


/**
 * Converts a string for use as part of a URL
 *
 * Basically just removes anything that's not alphanumeric, a hyphen or an underscore,
 * replacing it with a hyphen - although it's slightly better than that
 * @param  [type] $s [description]
 * @return [type]    [description]
 */
function urlify($s) {
	// the operations are applied from bottom to top
	return trim(            	                                    	                                  										// trim leading/trailing hyphens
		preg_replace('/-+/','-',	                                    	                                  				// reduce multiple hyphens to a single hyphen
			preg_replace('/[^a-z0-9_\-]+/i','-',	                                  	// convert anything else odd to a hyphen
				preg_replace('/["\',.’]+/i','',$s)		// remove certain characters
			)
		)
	,'-');
}

/**
 * A little debug function to Growl messages
 * @param  Mixed  $message Any variable
 * @param  boolean $sticky  If false, the message will disappear after a few seconds
 * @param  boolean $limit   Avoids too many Growls
 * @param  string  $title   Set the Growl message title
 * @return void
 */
function growl($message,$sticky = false,$limit = true,$title = '') {
	if (!DEBUGGING) return;
	static $growlCount = 0;
	$growlCount++;
	if ($growlCount>7 && $limit) {
		if ($growlCount==8)
			$message = '*** GROWL LIMIT REACHED ***';
		else
			return; // to avoid crashes, you can only growl 10 times
	}

	if ($sticky)
		$options .= ' -s';

	if ($message==='')
		$message = '(empty string)';

	if (is_null($message)) {
		$message = 'NULL';
	} elseif (is_bool($message)) {
		$message = ($message) ? 'TRUE' : 'FALSE';
	} elseif (!is_string($message)) {
		$message = print_r($message,true);
	}

	if (strlen($message)>1024)
		$message = substr($message, 0, 1021).'...';

	if (file_exists('/usr/local/bin/growlnotify'))
		shell_exec(sprintf('/usr/local/bin/growlnotify%s -m %s %s',
							$options,
							escapeshellarg($message),
							escapeshellarg($title)
						));
}

?>