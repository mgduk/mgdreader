<?php

namespace DolanReader;

/**
 * Singleton pattern for single database connection
 */
class Db {

    protected static $connection;

    /**
     * Protected constructor - use DB::get() to get the connection
     */
	private function __construct () {}

    /**
     * Returns a singleton
     * @return App object
     */
    public static function get () {
        if (!self::$connection) {
            try {
                self::$connection = new \PDO("mysql:host=".Config::get('database','server').";dbname=".Config::get('database','database'),
                            Config::get('database','user'),
                            Config::get('database','password')
                        );
            } catch(\PDOException $e) {
                new ErrorView(500,'Unable to establish a database connection');
            }
        }
        return self::$connection;
    }

}

?>