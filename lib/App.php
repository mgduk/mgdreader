<?php

namespace DolanReader;

class App extends Router {

    /**
     * Loads the requested Feed, dying with an ErrorView if it's not available
     *
     * @param  integer $feedId Feed ID
     * @return Feed
     */
    protected function getFeedForView ($feedId) {
        $feed = new Feed($feedId);
        if (!$feed->id)
            new ErrorView(404,'Feed not available');

        return $feed;
    }

    /**
     * Loads the requested FeedItem, dying with an ErrorView if it's not available
     *
     * @param  integer $itemId FeedItem ID
     * @return Feed
     */
    protected function getItemForView (Feed $feed,$itemId) {
        $item = $feed->getItem($itemId);
        if (!$item)
            new ErrorView(404,'Feed item not available');

        return $item;
    }

    public function feedView ($feedId) {
        $feed = $this->getFeedForView($feedId);
        $this->ensureCanonicalUrl($feed);
        return new PageView('feed',array('feed'=>$feed));
    }

    public function feedEditView ($feedId) {
        $feed = $this->getFeedForView($feedId);
        $this->ensureCanonicalUrl($feed,'/edit');
        return new PageView('feed_edit',array('feed'=>$feed));
    }

    public function feedItemView ($feedId,$itemId) {
        $feed = $this->getFeedForView($feedId,false);
        $item = $this->getItemForView($feed,$itemId);
        $this->ensureCanonicalUrl($item);
        return new PageView('feed_item',array('feed'=>$feed,'item'=>$item));
    }

    public function listView () {
        return new PageView('feed_list');
    }

    /**
     * Ensure URL is exactly as it should be by redirecting if it's not.
     * @param  Viewable $object e.g. a Feed or FeedItem
     * @param string $extension Extension to the Object's URL
     * @return void
     */
    public function ensureCanonicalUrl (Viewable $object,$extension = '') {
        $url = $object->getUrl().$extension;
        if ($url && $this->url!=$url) {
            new Redirect($url,301);
        }
    }

    /**
     * Throw exception on PHP error
     * @param  integer $code
     * @param  string $message
     * @param  string $file
     * @param  integer $line
     * @return void
     */
    public static function errorHandler ($code, $message, $file, $line) {
        switch ($code) {
            case E_NOTICE:
                return;
        }
        throw new \ErrorException($message, $code, 0, $file, $line);
    }
}

?>