<?php
namespace DolanReader;

/**
 * Formats some output to be returned as the response to an API call
 */
class ApiErrorResponse extends ApiResponse {

    public function __construct ($code,$message = null,$headers = array()) {
    	if ($message==null) {
    		switch ($code) {
    			case 404: $message = 'Not found'; break;
    		}
    	}
        $output = array('message'=>$message,'messageType'=>'error');
        parent::__construct($code,$output,$headers);
    }
}

?>