<?php
namespace DolanReader;

/**
 * Formats some output to be returned as the response to an API call,
 * and returns the appropriate code
 */
class ApiResponse {

    const FORMAT_JSON = 0;
    const FORMAT_XML = 1;
    // for use within the app, always redirects to a URL (possibly setting
    // a message to be displayed to the user) to rather than returning a JSON/XML
    const FORMAT_REDIRECT = 2;

    protected $format = self::FORMAT_JSON;

    protected $code;
    protected $output;
    protected $headers = array();

    protected $redirect;

    protected function setFormat ($format) {
        switch ($format) {
            case self::FORMAT_JSON:
            case self::FORMAT_XML:
            case self::FORMAT_REDIRECT:
                $this->format = $format;
                break;
            // ignore invalid formats
        }
    }

    public function output () {

        foreach ($this->headers as $key=>$value) {
            if ($key=='Location' && is_a($value, 'DolanReader\Viewable')) {
                if ($this->format==self::FORMAT_REDIRECT) {
                    // if we're in 'redirect format', and there's a location to go to, ensure we are
                    // using a code that will cause browsers to follow the location
                    if ($this->code<300)
                        $this->code = 303;
                    $value = $value->getUrl();
                } else {
                    $value = $value->getApiUrl();
                }
            }
            header($key.': '.$value);
        }

        switch ($this->code) {
            case 200: header('HTTP/1.1 200 OK'); break;
            case 201: header('HTTP/1.1 201 Created'); break;
            case 202: header('HTTP/1.1 202 Accepted'); break;
            case 204: header('HTTP/1.1 204 No Content'); break;

            case 300: header('HTTP/1.1 300 Multiple Choices'); break;
            case 301: header('HTTP/1.1 301 Moved Permanently'); break;
            case 302: header('HTTP/1.1 302 Found'); break;
            case 303: header('HTTP/1.1 303 See Other'); break;
            case 304: header('HTTP/1.1 304 Not Modified'); break;

            case 400: header('HTTP/1.1 400 Bad Request'); break;
            case 401: header('HTTP/1.1 401 Unauthorized'); break;
            case 403: header('HTTP/1.1 403 Forbidden'); break;
            case 404: header('HTTP/1.1 404 Not Found'); break;
            case 405: header('HTTP/1.1 405 Method Not Allowed'); break;
            case 406: header('HTTP/1.1 406 Not Acceptable'); break;
            case 408: header('HTTP/1.1 408 Request Timeout'); break;
            case 409: header('HTTP/1.1 409 Conflict'); break;
            case 410: header('HTTP/1.1 410 Gone'); break;
            case 411: header('HTTP/1.1 411 Length Required'); break;
            case 412: header('HTTP/1.1 412 Precondition Failed'); break;
            case 413: header('HTTP/1.1 413 Request Entity Too Large'); break;
            case 414: header('HTTP/1.1 414 Request-URI Too Long'); break;
            case 415: header('HTTP/1.1 415 Unsupported Media Type'); break;
            case 416: header('HTTP/1.1 416 Requested Range Not Satisfiable'); break;
            case 417: header('HTTP/1.1 417 Expectation Failed'); break;
            case 418: header('HTTP/1.1 418 I\'m a teapot'); break;
            case 426: header('HTTP/1.1 426 Upgrade Required'); break;

            case 500: header('HTTP/1.1 500 Internal Server Error'); break;
            case 501: header('HTTP/1.1 501 Not Implemented'); break;
            case 502: header('HTTP/1.1 502 Bad Gateway'); break;
            case 503: header('HTTP/1.1 503 Service Unavailable'); break;
            case 504: header('HTTP/1.1 504 Gateway Timeout'); break;
        }

        switch ($this->format) {
            case self::FORMAT_JSON:
                header('Content-type: application/json');
                print json_encode($this->output);
                break;

            case self::FORMAT_XML:
                // not implemented
                break;

            case self::FORMAT_REDIRECT:
                if (is_array($this->output))
                    new Message($this->output['message'],$this->output['messageType']);

                if ($this->redirect) {
                    new Redirect($this->redirect);
                }
                break;
        }
    }

    public function __set ($var,$value) {
        switch ($var) {
            case 'format': $this->setFormat($value); break;
        }
    }

    public function __construct ($code,$output,$headers = array()) {
        $this->code = $code;
        if (is_array($headers))
            $this->headers = $headers;
        if (is_string($output))
            $output = array('message'=>$output);
        $this->output = $output;
        // for use within the local app, a redirect location can be specified
        if (isset($_REQUEST['redirect'])) {
            $this->redirect = $_REQUEST['redirect'];
            $this->format = self::FORMAT_REDIRECT;
        }
    }
}

?>