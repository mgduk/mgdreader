<?php
namespace DolanReader;

/**
 * The Router class handles routing of URLs to functions
 *
 * See htdocs/api.php for routing configuration
 */
abstract class Router {

	const ERROR_CLASS = 'DolanReader\ErrorView';

	protected $baseURL;

	protected $url;
	protected $method;

	protected $routes = array();

	/**
	 * Register a responder for a specific URL pattern
	 * @param  string $method   get, post, put, delete, etc
	 * @param  string $pattern  Regular expression to match URL.
	 *                          Must match whole URL (after baseURL is prepended).
	 *                          Deliminators are backticks ``
	 * @param  callback $callback Function to deal with the request.
	 *                            Receives matched groups from $pattern as arguments.
	 * @return void
	 */
	public function addRoute ($pattern,$callback,$method = 'get') {
		if (!$this->routes[$method])
			$this->routes[$method] = array();

		// add deliminators and anchors to regex
		$pattern = '`^'.$this->baseURL.$pattern.'$`';

		$this->routes[$method][$pattern] = $callback;
	}


	/**
	 * Route request URL and call callback
	 * @return void
	 */
	public function route ($url,$method = 'get') {
		$this->url = $url;
		$this->method = strtolower($method);

		$errorClass = static::ERROR_CLASS;

		if (is_array($this->routes[$this->method]))
			foreach ($this->routes[$this->method] as $pattern=>$callback) {

				if (preg_match($pattern, $this->url,$matches)) {
					// remove the whole string match
					array_shift($matches);
					try {
						return call_user_func_array($callback, $matches);
					} catch (\Exception $e) {
						return new $errorClass(500,$e->getMessage());
					}
				}
			}
		return new $errorClass(404,'Not Found');
	}

	/**
	 * Constructor
	 * @param string $baseURL Appended to the start of route patterns.
	 */
	public function __construct ($baseURL) {
		$this->baseURL = rtrim($baseURL,'/');
	}

}

?>