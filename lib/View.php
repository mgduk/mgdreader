<?php

namespace DolanReader;

/**
 *
 */
class View {

	protected $view;
	protected $vars;

	public function assign ($var,$value) {
		$this->vars[$var] = $value;
	}

	public function output () {
		extract($this->vars);
		include Config::getFolder('views').$this->view.'.php';
	}

	public function __construct ($view,$vars = array()) {
		$this->view = $view;
		$this->vars = (is_array($vars)) ? $vars : array();
	}
}

?>