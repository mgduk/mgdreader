<?php
namespace DolanReader;

class FeedItem implements Viewable {

    protected $feed;

    protected $id;
    protected $guid;
    protected $title;
    protected $link;
    protected $description;
    protected $image;
    protected $date;

    protected $enclosures;

    protected $dateAdded;

    public function __get ($var) {
        switch ($var) {
            case 'feed': return $this->feed;
            case 'id': return $this->id;
            case 'guid': return $this->guid;
            case 'title': return $this->title;
            case 'link': return $this->link;
            case 'description': return $this->description;
            case 'image': return $this->image;
            case 'date': return $this->date;
            case 'dateAdded': return $this->dateAdded;
            case 'dateString': return $this->getDateString();
            case 'url': return $this->getUrl();
            case 'apiUrl': return $this->getApiUrl();
            case 'enclosures': return $this->getEnclosures();
            case 'data': return $this->getData();
            case 'minimalData': return $this->getData(false);
        }
    }

    /**
     * Returns user-facing URL within app
     * @return string
     */
    public function getUrl () {
        return $this->feed->url.'/'.$this->id.'/'.urlify(strtolower($this->title));
    }

    /**
     * Returns the API endpoint for this feed
     * @return string
     */
    public function getApiUrl () {
        return '/api/items/'.$this->id;
    }

    /**
     * Returns a friendly string representation of the date
     *
     * If today, just includes the time.
     * If yesterday, includes 'yesterday'
     * Otherwise, include the full date and time
     * @return string
     */
    protected function getDateString () {
        if (!$this->date) return '';

        $now = new \DateTime();
        // today - just return the time
        if ($this->date->format('dmY') == $now->format('dmY'))
            return $this->date->format(Config::get('timeFormat'));
        elseif ($this->date->format('dmY')==$now->sub(new \DateInterval('P1D'))->format('dmY'))
            return 'yesterday '.$this->date->format(Config::get('timeFormat'));
        else
            return $this->date->format(Config::get('dateTimeFormat'));
    }

    /**
     * Returns a array of Enclosure objects associated with this Item
     * @return array of Enclosure
     */
    protected function getEnclosures () {
        if (!$this->enclosures) {
            $db = Db::get();
            $query = $db->prepare("SELECT * FROM `enclosures` WHERE `item` = :item ORDER BY `id`");
            $query->execute(array('item'=>$this->id));
            $this->enclosures = array();
            while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
                $enclosure = new Enclosure($this);
                $enclosure->data = $row;
                $this->enclosures[] = $enclosure;
            }
        }
        return $this->enclosures;
    }

    /**
     * Returns an array representing this FeedItem's data
     * @return array
     */
    protected function getData ($full = true) {
        $data = array(
            'id' => (integer)$this->id,

            'guid' => $this->guid,
            'title' => $this->title,
            'image' => $this->image,
            'date' => ($this->date)?$this->date->format('c'):null,
            'dateAdded' => ($this->dateAdded)?$this->dateAdded->format('c'):null,

            'url' => $this->getApiUrl(),
            'feed' => $this->feed->apiUrl

        );
        if ($full) {
            $data['link'] = $this->link;
            $data['description'] = $this->description;
            $data['enclosures'] = array_map(function($obj){return $obj->data;},$this->getEnclosures());
        }
        return $data;
    }


    public function addEnclosure (Enclosure $enclosure) {
        $this->enclosures[] = $enclosure;
    }

    /**
     * Loads the data from $data into the object's properties
     *
     * The data will be coming either from a database call or XML parse
     * @param array $data
     */
    public function setData (array $data) {
        if (isset($data['id']))
            $this->id = $data['id'];
        $this->title = $data['title'];
        $this->link = $data['link'];
        $this->description = $data['description'];
        $this->image = $data['image'];

        if (isset($data['date']))
            $this->date = new \DateTime($data['date']);
        if (isset($data['pubDate']))
            $this->date = new \DateTime($data['pubDate']);

        if (isset($data['guid']))
            $this->guid = $data['guid'];
        elseif ($this->link)
            $this->guid = $this->link;

        if (isset($data['dateAdded']))
            $this->dateAdded = new \DateTime($data['dateAdded']);

        // in the absence of a supplied guid, generate one by hashing the content
        if (empty($this->guid))
            $this->guid = sha1($this->title.$this->description);
    }


    /**
     * Save to database
     * @return [type] [description]
     */
    public function save () {
        $db = Db::get();
        $query = $db->prepare("INSERT INTO items (
                                        `feed`,
                                        `guid`,
                                        `title`,
                                        `link`,
                                        `description`,
                                        `image`,
                                        `date`,
                                        `dateAdded`
                                    )
                                    VALUES (
                                        :feed,
                                        :guid,
                                        :title,
                                        :link,
                                        :description,
                                        :image,
                                        :date,
                                        NOW()
                                    )
                                    ON DUPLICATE KEY UPDATE
                                    `title`=:title,
                                    `link`=:link,
                                    `description`=:description,
                                    `image`=:image
                                ");
        $success = $query->execute(array(
                'feed' => $this->feed->id,
                'guid' => $this->guid,
                'title' => $this->title,
                'link' => $this->link,
                'description' => $this->description,
                'date'=>($this->date)?$this->date->format('Y-m-d H:i:s'):null,
                'image'=>$this->image
            ));
        if ($db->lastInsertId())
            $this->id = $db->lastInsertId();

        return $success;
    }

    public function __construct (Feed $feed,$data = null) {
        $this->feed = $feed;
        if ($data)
            $this->setData($data);
    }

    /**
     * Gets a FeedItem by ID
     * @param  integer $id
     * @return FeedItem
     */
    public static function get ($id) {
        $db = Db::get();
        $query = $db->prepare("SELECT * FROM `items` WHERE `id` = :item LIMIT 1");
        $success = $query->execute(array('item'=>$id));
        if (!$success || !$query->rowCount()) return false;
        $row = $query->fetch(\PDO::FETCH_ASSOC);
        if ($row) {
            $feed = new Feed($row['feed']);
            $item = new FeedItem($feed,$row);
        }
        return $item;
    }
}

?>