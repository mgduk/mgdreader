<?php
namespace DolanReader;

/**
 * The ApiRouter class handles routing of URLs to functions
 *
 * See htdocs/api.php for routing configuration
 */
class ApiRouter extends Router {

	const ERROR_CLASS = 'DolanReader\ApiErrorResponse';

}

?>