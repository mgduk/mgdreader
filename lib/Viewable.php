<?php

namespace DolanReader;

/**
 * Defines an interface for objects that produce view(s) accessible at URL(s)
 */
interface Viewable {

	public function getUrl();

	public function getApiUrl();

}

?>