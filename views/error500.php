<h1>Oh dear - something went wrong</h1>
<p><?=$this->message?></p>
<p>We're sorry about that. Please try again, or get in touch if the problem continues.</p>