<?php
namespace DolanReader;
?>
<article>
<header>
    <h1><a href="<?=$feed->url?>"><?=htmlspecialchars($feed->title)?></a></h1>
</header>
	<form action="/api/feeds/<?=$feed->id?>" method="post" class="feed_edit">
		<h2>Edit Details</h2>
		<input type="hidden" name="_method" value="put">
		<div class="row">
			<label for="editFeed_rssUrl">RSS Feed URL</label>
			<input type="url" name="rssUrl" value="<?=$feed->rssUrl?>" id="editFeed_rssUrl">
		</div>
		<div class="row">
			<label for="editFeed_title">Title</label>
			<input type="text" name="title" value="<?=$feed->title?>" id="editFeed_title">
		</div>
		<div class="row">
			<label for="editFeed_description">Description</label>
			<textarea name="description" id="editFeed_description"><?=$feed->description?></textarea>
		</div>
		<div class="row">
			<label for="editFeed_image">Image URL</label>
			<input type="url" name="image" value="<?=$feed->image?>" id="editFeed_image">
		</div>
		<input type="hidden" name="redirect" value="<?=$feed->url?>">
		<button type="submit">Save</button>
	</form>

	<form action="/api/feeds/<?=$feed->id?>" method="post" class="feed_delete">
		<h2>Delete Feed</h2>
		<p><strong>Be careful now, this cannot be undone</strong></p>
		<input type="hidden" name="_method" value="delete">
		<input type="hidden" name="redirect" value="/">
		<button>Delete <?=htmlspecialchars($feed->title)?> feed</button>
	</form>
</article>