<?php namespace DolanReader; ?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html>
<head>
    <meta charset="utf-8">

    <title><? print htmlspecialchars($this->title); ?></title>

    <link rel="stylesheet" href="/skin/screen.css" />

</head>
<body>
<header>
	<a id="logo" href="<?=Config::get('rootURL')?>">DolanReadr</a>
</header>
<?
// print any messages as needed
print implode('',Message::get());

print $this->content;
?>
</body>
</html>