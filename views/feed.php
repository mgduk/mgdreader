<?php
namespace DolanReader;
?>
<article class="feed">
    <header>

        <div class="feed_meta">
            <? if ($feed->image): ?><img src="<?=$feed->image?>" alt=""><? endif; ?>
            <h1><?=htmlspecialchars($feed->title)?></h1>
            <p class="feed_description"><?=htmlspecialchars($feed->description)?></p>
        </div>

        <div class="feed_toolbar">
            <a href="<?=$feed->url?>/edit" class="feed_edit_button">Edit details</a>
            <form action="/api/feeds/<?=$feed->id?>/updates" method="post" class="feed_update">
                <? if ($feed->lastUpdate): ?><div>Last updated <?=$feed->lastUpdate->date->format(Config::get('dateTimeFormat'))?></div><? endif; ?>
                <input type="hidden" name="redirect" value="<?=$feed->url?>">
                <button>Update now</button>
            </form>
        </div>
    </header>
    <? print implode('',Message::get()); ?>

    <ul class="feed_items list">
    <?php foreach ($feed->items as $item): ?>
        <li>
        	<span class="date"><?=$item->dateString?></span>
        	<a href="<?=$item->url?>">
        	<? if ($item->image): ?>
        		<img src="<?=$item->image?>" alt="">
        	<? endif; ?>
        	<span class="title"><?=htmlspecialchars($item->title)?></span>
        	</a>
    	</li>
    <?php endforeach; ?>
    </ul>

    <p class="copyright"><?=$feed->copyright?></p>
</article>