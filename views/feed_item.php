<?php
namespace DolanReader;
?>
<article class="feed_item">
	<header>
	    <div class="back_to_feed">Back to <a href="<?=$feed->url?>"><?=htmlspecialchars($feed->title)?></a></div>
	</header>

<h1><?=htmlspecialchars($item->title)?></h1>
<div class="date"><?=$item->date->format(Config::get('dateTimeFormat'))?></div>
<div class="description">
	<? if ($item->image): ?><img src="<?=$item->image?>" alt="" class="feed_item_thumbnail"><? endif; ?>
	<?=$item->description?>
</div>
<? if ($item->enclosures): ?>
	<div class="enclosures">
	<? foreach ($item->enclosures as $enclosure):
		if ($enclosure->url==$item->link) continue;
		switch ($enclosure->type):
			case 'image/jpeg': ?>
				<img src="<?=$enclosure->url?>" alt="">
				<?
				break;
			default: ?>
				<a href="<?=$enclosure->url?>"><?=$enclosure->title?></a>
				<?
		endswitch;
	endforeach; ?>
	</div>
<? endif; ?>
<a href="<?=$item->link?>" class="item_link"><?=htmlspecialchars($item->title)?></a>
</article>