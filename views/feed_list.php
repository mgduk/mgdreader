<?php
namespace DolanReader;
?>
<form action="/api/feeds" method="post" class="feed_add">
    <input type="hidden" name="redirect" value="">
    <label for="add_feed">Add a feed</label>
    <input type="url" id="add_feed" name="url" />
    <button>Add</button>
</form>

<article>
<?
$feeds = Feed::getAll();

if (count($feeds)): ?>
    <ul class="feeds_list list">
        <? foreach ($feeds as $feed): ?>
            <li>
                <a href="<?=$feed->url?>">
                    <span class="imageWrap"><? if ($feed->image): ?><img src="<?=$feed->image?>" alt=""><? endif; ?></span>
                    <span class="title"><?=htmlspecialchars($feed->title)?></span>
                </a>
            </li>
        <? endforeach; ?>
    </ul>
<? else: ?>
<p>Just enter the URL of your RSS feed above.</p>
<? endif; ?>
</article>