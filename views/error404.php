<h1>Not found</h1>
<p><?=$this->message?></p>
<p>Sorry, we can't give you what you wanted this time - we just can't find it! (That's a 404 for techy types)</p>
<p>Why not <a href="<?=DolanReader\Config::get('rootURL')?>">return to the homepage</a>.</p>