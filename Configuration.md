Configuration
-------------
You can configure the app using the user-config.json file in the root.
Anything defined in this JSON-format file overrides that defined in default-config.json.
DO NOT EDIT default-config.json, as this will be reset when the app is updated. Changes to
your user-config.json file will be preserved.


{
	// database connection info
	"database": {
		"server": "localhost",
		"user": "dolanreadr",
		"password": "",
		"database": "dolanreadr"
	},

	// the location of the folders, relative to the app root
	"folders": {
		"lib": "lib",
		"views": "views"
	},

	// the URL of the app
	"rootURL": "/",

	// the title to be displayed - allows for white labelling
	"title": "DolanReadr",

	// format dates and times - for syntax see http://php.net/date
	"timeFormat": "h:ia",
	"dateFormat": "D jS M Y",
	"dateTimeFormat": "D jS M Y h:ia",

	// feed items more than this many days old will be deleted when updating a feed
	"maxAge": 90

	// set this to true to receive more verbose error output
	"debugMode": true
}
