# ************************************************************
# Sequel Pro SQL dump
# Version 4004
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.29)
# Database: mgdreader
# Generation Time: 2013-07-14 22:42:26 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table enclosures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `enclosures`;

CREATE TABLE `enclosures` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item` int(10) unsigned DEFAULT NULL,
  `url` varchar(512) DEFAULT NULL,
  `length` bigint(20) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table feeds
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feeds`;

CREATE TABLE `feeds` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rssUrl` varchar(512) CHARACTER SET latin1 DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `link` varchar(512) CHARACTER SET latin1 DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `language` varchar(16) DEFAULT NULL,
  `pubDate` datetime DEFAULT NULL,
  `lastBuildDate` datetime DEFAULT NULL,
  `copyright` varchar(512) DEFAULT NULL,
  `image` varchar(512) CHARACTER SET latin1 DEFAULT NULL,
  `dateAdded` datetime DEFAULT NULL,
  `lastUpdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rssFeedUrl` (`rssUrl`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `feed` int(11) DEFAULT NULL,
  `guid` varchar(512) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `title` varchar(512) DEFAULT NULL,
  `link` varchar(512) CHARACTER SET latin1 DEFAULT NULL,
  `description` text,
  `image` varchar(512) CHARACTER SET latin1 DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `dateAdded` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `feed` (`feed`,`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table updates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `updates`;

CREATE TABLE `updates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `feed` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `itemCount` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
