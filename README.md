An RSS client.

Features
--------
* Add feeds by URL
* Edit feed details/delete feeds
* Prunes old items (max age is configurable)
* Displays images for feed and items if supplied
* REST api

Responsive design
-----------------

Designed to work best in a narrow screen (e.g. mobile, or in a small browser window), but adapts to larger viewports.

Exposes a REST API to enable building upon this app.
e.g. You could write a native mobile app (or Android widget, perhaps) to retrieve the data via the REST API, which would keep the mobile app and in sync.
